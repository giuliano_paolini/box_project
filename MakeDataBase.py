# -*- coding: utf-8 -*-

import pandas as pd 
import os.path as path
import shutil

#The excel file "UltimosMovimientos.xls" is read as a DataFrame. The Header is defined in the 14th row 
base= pd.read_excel("Movements/UltimosMovimientos.xls", sheet_name='UltimosMovimientos', header=14) 

#All columns (axis=1) and rows (axis=0) which contain all values NA are removed  
base2 = base.dropna(axis=1,how='all')
base2 = base2.dropna(how='all')

#All existing values NA are filled with 0
base3= base2.fillna(0)

#The content of the some variables are transformed to a numeric value
base3["Cuenta sueldo"] = base3["Cuenta sueldo"].str.replace('.','').str.replace(',','.')
base3["Cuenta sueldo"]= pd.to_numeric(base3['Cuenta sueldo'], errors= 'raise')

base3["Importe cuenta corriente pesos"] = base3["Importe cuenta corriente pesos"].str.replace('.','').str.replace(',','.')
base3["Importe cuenta corriente pesos"]= pd.to_numeric(base3['Importe cuenta corriente pesos'], errors= 'raise')

base3["Saldo pesos"] = base3["Saldo pesos"].str.replace('.','').str.replace(',','.')
base3["Saldo pesos"]= pd.to_numeric(base3['Saldo pesos'], errors= 'raise')

base3= base3.fillna(0)

#New variables are created from existing variables 
base3["Cuenta"]=(base3["Cuenta sueldo"]+ base3["Importe cuenta corriente pesos"])

base3["Descripción"] = base3["Descripción"].str.replace('%','prc')

sarray= base3["Descripción"].str.split("  ", 1, expand= True) #The string are splitted in a new array of strings 

base3["Grupo"]= sarray[:][0] #The elements in the column 0 of the new array are saved in a new variable called Grupo

base3["Detalle"]= sarray[:][0].str.replace(' ','')

#Fecha is transformed in a datetime data
base3["Fecha"]= pd.to_datetime(base3["Fecha"])

base3["Etapa del mes"]= [1 if i<11 else 3 if i>20 else 2 for i in base3["Fecha"].dt.day]

base3["Mes"]= base3["Fecha"].dt.month

base3["Año"]= base3["Fecha"].dt.year

#Some variables are droped
base3= base3.drop(['Sucursal origen','Descripción'], axis=1)

#A new database is created or actualiced
if path.exists("DataBase/DataBase.xls"):
    
    base4= pd.read_excel("DataBase/DataBase.xls", sheet_name="Base")
    
    print(base4.dtypes)
    print(base4.head())
    
    base4= base4.append(base3,ignore_index=True).drop_duplicates()
    
    base4.to_excel("DataBase/DataBase.xls", sheet_name="Base", index= False)
    
    
    
else:
    
    base3.to_excel("DataBase/DataBase.xls", sheet_name="Base", index= False)
    

shutil.move("Movements/UltimosMovimientos.xls", "Repository/UltimosMovimientos.xls")
